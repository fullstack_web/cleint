(function () {
    'use strict';

    /* JAVASCRIPT */

    /**
     * Task Object/function
     */
    function Task () {

        /***************** PRIVATE *******************/

        //always bind to an object.property
        var _model = {
          tasks:[{id:1,name:'one'}],//,{id:2,name:'two'}],
          task: null
        };

        function _clear(){
          _model.task={
            id:10,
            name:null
          };
        }
        function _add(){
          var newTask ={id:1,name:_model.task};
          _model.tasks.push(newTask);
        }
        /**
         * doSomething() - Private function
         */
          /**function _doSomething() {
            // add logic here...*/
        //}

        /****************** PUBLIC *******************/
        var service = {
            add : _add,
            model:_model
        };

        return service;

    }

    /* ANGULAR */
    angular
        .module('home')
        .factory('task', Task );

})();
