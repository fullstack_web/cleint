(function () {
    'use strict';

    angular.module('home', []);

    angular
        .module('home')
        .config(function ($stateProvider) {

            $stateProvider.state('shell.home', {
                url: '/home',
                views: {
                    'contentView': {
                        /**templateUrl: 'home/templates/home-template.html',
                        controller: 'homeController',
                        controllerAs: 'vm'*/
                        template: '<home-container></home-container>'
                    },
                    'homeAddView@shell.home':{
                      template:'<task-add></task-add>'
                      //angular2=component:task-add
                    },
                    'homeListView@shell.home':{
                      template:'<task-list tasks="vm.model.tasks"></task-list>'
                    }
                }

            });
            /* Add New States Above */

        });

})();
