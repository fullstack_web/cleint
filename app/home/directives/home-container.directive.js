(function () {
    'use strict';


    /**
     * HomeContainer Object/function
     */
    function HomeContainer () {

        /***************** PRIVATE *******************/

        /**
         * Directives link function
         */
        function _link(scope, iElem, iAttrs, controllers) {
            // add logic here
        }

        /* JAVASCRIPT */
        function HomeController(homeService,task,commonService) {

            // vm (view-model) is the object we bind to (this controller).
            var vm = this;

            /***************** PRIVATE *******************/
            var _name = 'HomeController';

            /**
             * getName() - Private function
             */
            function _getName(val) {
                return _name;
            }
            function _onSend(){
              commonService.doSomething();
            }
            /****************** PUBLIC *******************/
            vm.onSend = _onSend;
            vm.getName = _getName;
            vm.service = homeService;
            vm.model = task.model;
        }

        /****************** PUBLIC *******************/
        var directive = {
            restrict: 'E',
            replace: true,
            scope: {

            },
            templateUrl: 'home/directives/home-container.directive.html',
            link: _link,
            controller:HomeController,
            controllerAs:'vm'
        };

        return directive;

    }

    /* ANGULAR */
    angular
        .module('home')
        .directive('homeContainer', HomeContainer );

})();
