(function () {
    'use strict';

    /* JAVASCRIPT */

    /**
     * TaskAdd Object/function
     */
    function TaskAdd () {

        /***************** PRIVATE *******************/
        function TaskAddController(task){
          var vm = this;
          vm.model = task.model;
          vm.add = task.add;
          //vm.add =function(){
          //  vm.onAdd();
          //}
        }
        /**
         * Directives link function
         */
        function _link(scope, iElem, iAttrs, controllers) {
            // add logic here
        }

        /****************** PUBLIC *******************/
        var directive = {
            restrict: 'E',
            replace: true,
            scope: {
            //  tasks:'=',
            //  onAdd:'&'
            },
            templateUrl: 'home/directives/task-add/task-add.directive.html',
            link: _link,
            controller: TaskAddController,
            controllerAs: 'vm',
            bindToController:true
        };

        return directive;

    }

    /* ANGULAR */
    angular
        .module('home')
        .directive('taskAdd', TaskAdd );

})();
