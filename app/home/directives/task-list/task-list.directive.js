(function () {
    'use strict';

    /* JAVASCRIPT */

    /**
     * TaskList Object/function
     */
    function TaskList () {

        /***************** PRIVATE *******************/

        /**
         * Directives link function
         */
        function _link(scope, iElem, iAttrs, controllers) {
            // add logic here
        }

        /****************** PUBLIC *******************/
        var directive = {
            restrict: 'E',
            replace: true,
            scope: {
              tasks:'='//@
            },
            templateUrl: 'home/directives/task-list/task-list.directive.html',
            link: _link,
            controller:TaskListController,
            controllerAs :'vm',
            bindToController:true
        };

        return directive;

    }
    function TaskListController(task) {

        // vm (view-model) is the object we bind to (this controller).
        var vm = this;
        //vm.model = task.model;
        /****************** PUBLIC *******************/
    }
    /* ANGULAR */
    angular
        .module('home')
        .directive('taskList', TaskList );

})();
