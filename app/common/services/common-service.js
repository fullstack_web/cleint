(function () {
    'use strict';

    /* JAVASCRIPT */

    /**
     * CommonService Object/function
     */
    function CommonService($http) {

        /***************** PRIVATE *******************/

        //always bind to an object.property
        var _model = {
            data: null
        };

        /**
         * doSomething() - Private function
         */
        function _doSomething() {
          return $http({
              method : "GET",
              url : "home/json-file/Text.json"
          }).then(function mySucces(response) {
              var r = response;
          }, function myError(response) {
              var r = response;
          });
        }

        /****************** PUBLIC *******************/
        var service = {
            doSomething: _doSomething,
            get data() {
                return _model.data;
            },
            set data(val) {
                _model.data = val;
            }
        };

        return service;

    }

    /* ANGULAR */
    angular
        .module('common')
        .factory('commonService', CommonService);

})();
